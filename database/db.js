const mysql = require("mysql2");
const { dbPort, host, user, password, database } = require("../config");

const db = mysql.createPool({
  connectionLimit: 100,
  port: dbPort,
  host: host,
  user: user,
  password: password,
  database: database,
});

module.exports = db;

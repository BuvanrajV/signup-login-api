const express = require("express");
const {
  getAllUsersController,
  createUserController,
  userLoginController,
  getUserController,
} = require("../controller/controller");

const router = express.Router();

const checkToken = (req, res, next) => {
  const token = req.headers["authorization"].split(" ")[1];
  if (token === null) {
    res.status(400).send({
      message: "Token can not be empty",
    });
  } else {
    req.token = token;
    next();
  }
};

const checkBody = (req, res, next) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  } else {
    next();
  }
};

router.get("/getUsers", getAllUsersController);
router.post("/signup", checkBody, createUserController);
router.post("/login", checkBody, userLoginController);
router.get("/login", checkToken, getUserController);

module.exports = router;

const express = require("express");
const bodyParser = require("body-parser");
const { port } = require("./config");
const routes = require("./routes/routes");

const app = express();
app.use(bodyParser.json());

app.use("/", routes);
app.get("/",(req,res)=>{
  res.send("Hello , Welcome")
})

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});

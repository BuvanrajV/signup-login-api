const bcrypt = require("bcrypt");
const database = require("../database/db");

const getAllUsersDetail = () => {
  return new Promise((resolve, reject) => {
    database.query("select * from loginDetails", (err, res) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        console.log("username : ", res);
        resolve(res);
      }
    });
  });
};

const createUser = (name, password) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, 10, (err, hashPassword) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        database.query(
          "Insert into loginDetails (username,password) values (?,?)",
          [name, hashPassword],
          (err, res) => {
            if (err) {
              console.error(err);
              reject(err);
            } else {
              console.log(`username : ${name} , password : ${password}`);
              resolve(res);
            }
          }
        );
      }
    });
  });
};

const userLogin = (username, password) => {
  return new Promise((resolve, reject) => {
    database.query(
      "Select * from loginDetails where username=?",
      [username],
      (err, res) => {
        if (err || res.length === 0) {
          console.error(err);
          reject(err);
        } else {
          bcrypt.compare(password, res[0].password, (error, response) => {
            if (error) {
              reject(error);
            } else {
              resolve(response);
            }
          });
        }
      }
    );
  });
};

const getUserDetail = (user) => {
  return new Promise((resolve, reject) => {
    database.query(
      "select * from loginDetails where username=?",
      [user.username],
      (err, res) => {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          resolve(res);
        }
      }
    );
  });
};

module.exports = {
  getAllUsersDetail,
  createUser,
  userLogin,
  getUserDetail,
};

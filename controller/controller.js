const jwt = require("jsonwebtoken");
const {
  getAllUsersDetail,
  createUser,
  userLogin,
  getUserDetail,
} = require("../models/usersDetailModel");
const { accessTokenSecret } = require("../config");

const getAllUsersController = (req, res) => {
  getAllUsersDetail()
    .then((usernames) => {
      res.send(usernames);
    })
    .catch((err) => {
      res.status(500).send({ message: err.message || "Some error occurred" });
    });
};

const createUserController = (req, res) => {
  createUser(req.body.username, req.body.password)
    .then(() => {
      res.send({ message: "Created Successfully" });
    })
    .catch((err) => {
      res.status(500).send({ message: "Username already Exist" });
    });
};

const userLoginController = (req, res) => {
  userLogin(req.body.username, req.body.password)
    .then((response) => {
      if (response) {
        const user = {
          username: req.body.username,
        };
        const accessToken = jwt.sign(user, accessTokenSecret);
        res.send({ accessToken });
      } else {
        res.status(400).send({ message: "Wrong Password" });
      }
    })
    .catch(() => {
      res.status(400).send({
        message: "Wrong Username",
      });
    });
};

const getUserController = (req, res) => {
  jwt.verify(req.token, accessTokenSecret, (error, user) => {
    if (error) {
      console.error(error);
      res.status(400).send({ message: "Invalid Token" });
    } else {
      getUserDetail(user)
        .then((response) => {
          res.send(response);
        })
        .catch((err) => {
          res
            .status(500)
            .send({ message: err.message || "Some error occurred" });
        });
    }
  });
};

module.exports = {
  getAllUsersController,
  createUserController,
  userLoginController,
  getUserController,
};
